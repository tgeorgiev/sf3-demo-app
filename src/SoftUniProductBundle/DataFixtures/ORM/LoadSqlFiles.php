<?php

namespace SoftUniProductBundle\DataFixtures\ORM;

use Symfony\Component\Finder\Finder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSqlFiles implements FixtureInterface, OrderedFixtureInterface
{

    public function getOrder() { return 1; }

    public function load(ObjectManager $manager)
    {

        /*
         * Execute all sql files from DataFixtures/sql folder
         */
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../sql/');

        foreach ($finder as $file) {

            $sql = $file->getContents();
            $manager->getConnection()->exec($sql);  // Execute native SQL
            $manager->flush();

            echo $file->getBasename('.sql') . '.sql is applied' . PHP_EOL;
        }

    }

}

