<?php

namespace SoftUniProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Tests\Fixtures\Entity;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use SoftUniProductBundle\Entity\Category;
use SoftUniProductBundle\Entity\Product;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('imageFile', VichImageType::class, [
                'required'  => false,
                'label'     => 'Category Image'
            ])
            ->add('rank')
            ->add('parent')
            ->add('products', EntityType::class, array(
                'multiple' => true,
                'expanded' => true,
                'class' => 'SoftUniProductBundle:Product'
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'softuniproductbundle_category';
    }


}
