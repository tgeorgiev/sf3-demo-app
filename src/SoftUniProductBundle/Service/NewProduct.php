<?php

namespace SoftUniProductBundle\Service;

use SoftUniProductBundle\Entity\Product;

class NewProduct
{
    protected $mailer;
    protected $templating;

    public function __construct(\Swift_Mailer $mailer, $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendEmail($toEmails)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('SoftUni Demo App - Added New Product')
            ->setFrom('send@example.com')
            ->setTo($toEmails)
            ->setBody($this->templating->render('product/new_product_email.html.twig'), 'text/html')
        ;
        $this->mailer->send($message);
    }

}

