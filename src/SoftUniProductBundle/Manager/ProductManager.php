<?php

namespace SoftUniProductBundle\Manager;

use Doctrine\ORM\EntityManager;

use SoftUniProductBundle\Entity\Product;
use Symfony\Component\HttpFoundation\RequestStack;


class ProductManager
{
    protected $em = null;
    protected $requestStack = null;
    protected $request = null;
    protected $session = null;
    protected $cookies = null;
    protected $class = null;
    protected $repository = null;
    protected $container = null;

    /**
     * @param EntityManager $em
     * @param RequestStack $request_stack
     * @param $class
     */
    public function __construct(EntityManager $em, RequestStack $request_stack, $class, $container)
    {
        $this->em = $em;
        $this->requestStack = $request_stack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->session = $this->request->getSession();
        $this->cookies = $this->request->cookies;
        $this->class = $class;
        $this->container = $container;
        $this->repository = $this->em->getRepository($class);

    }

    public function getClass()
    {
        return $this->class;
    }

    public function createProduct()
    {
        $class = $this->getClass();
        return new $class;
    }

    public function removeProduct(Product $product)
    {
        $this->em->remove($product);
        $this->em->flush();
        return true;
    }

    public function findProductBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }
}