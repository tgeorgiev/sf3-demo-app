<?php

namespace SoftUniProductBundle\Manager;

use Doctrine\ORM\EntityManager;
use SoftUniProductBundle\Entity\Category;


class CategoryManager
{
    protected $em = null;
    protected $class = null;
    protected $repository = null;

    /**
     * @param EntityManager $em
     * @param $class
     */
    public function __construct(EntityManager $em, $class)
    {
        $this->em = $em;
        $this->class = $class;
        $this->repository = $this->em->getRepository($class);
    }

    public function createCategory()
    {
        $class = $this->getClass();
        return new $class;
    }

    public function removeCategory(Category $category)
    {
        $this->em->remove($category);
        $this->em->flush();
        return true;
    }

    public function findCategoryBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    public function getClass()
    {
        return $this->class;
    }
}