Symfony 3.2 Demo App
========================

This is a project on symfony for SoftUni.

What's inside?
--------------

It is an admin panel for Products and Categories
![screenshot](docs/sf3-demo.png "EasyAdmin")

Installation
--------------
You can clone it and run the following commands:
```
composer install
```

```
#php bin/console doctrine:database:drop --force
#php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

```
php bin/console doctrine:fixtures:load
```

Links
--------------
1. [Login page](http://sf3-demo.nutrapress.com/login)
2. [My Admin](http://sf3-demo.nutrapress.com/my-admin/product/)
3. [Easy Admin](http://sf3-demo.nutrapress.com/admin/)

Enjoy!
